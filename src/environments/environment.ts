export let environment = {
    production: false,
    configuration: {
        server: {
            protocol: 'http',
            host: 'localhost',
            port: '8080'
        }
    }
};
