export let environment = {
    production: false,
    configuration: {
        server: {
            protocol: 'http',
            host: 'sys-service-sys.apps.us-west-1.starter.openshift-online.com',
            port: '80'
        }
    }
};
