import {ErrorMessage} from 'ng-bootstrap-form-validation';

export const CUSTOM_ERRORS: ErrorMessage[] = [
  {
    error: 'required',
    format: requiredFormat,
  }, {
    error: 'email',
    format: emailFormat,
  }, {
    error: 'maxLength',
    format: maxLengthFormat,
  }, {
    error: 'ptDate',
    format: ptDateFormat,
  }
];

export function requiredFormat(label: string, error: any): string {
  return `Campo de preenchimento obrigatório!`;
}

export function ptDateFormat(label: string, error: any): string {
  return `Data inválida!`;
}

export function maxLengthFormat(label: string, error: any): string {
  return `Tamanho inválido.`;
}

export function emailFormat(label: string, error: any): string {
  return `${label} doesn't look like a valid email address.`;
}
