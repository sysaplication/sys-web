export class PageModel {
  constructor(size?: number) {
    this.size = size;
  }

  content: Array<any> = [];
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  page: number;
  first: boolean;
  numberOfElements: number;
}

