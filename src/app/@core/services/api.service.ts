import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ApiHttpService} from './api-http.service';

export class ApiService extends ApiHttpService {

    constructor(private httpClient: HttpClient, public path: string) {
        super(httpClient, path);
    }

    recuperarPorId(id: number): Observable<any> {
        return this.get(id);
    }

    cadastrar(objeto: any): Observable<any> {
        return this.post(objeto);
    }

    editar(objeto: any): Observable<any> {
        return this.put(objeto);
    }

    deletar(id: any): Observable<any> {
        return this.delete(id);
    }

    filtrar(filtro: any, page: string, size: string, sort: string): Observable<any> {
        let params = new HttpParams();
        params = params.append('page', page);
        params = params.append('size', size);

        if (sort) {
            params = params.append('sort', sort);
        } else {
            params = params.append('sort', 'dataAlteracao,desc');
        }

        return this.filterPost('', filtro, params);
    }
}
