import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';

export abstract class ApiHttpService {

    private API_URL: string;
    private readonly _http: HttpClient;
    private _basePath: string;
    protected configuration: any;

    protected constructor(http: HttpClient, basePath: string) {
        this._http = http;
        this._basePath = basePath;

        this.configuration = environment.configuration;
        this.API_URL = this.createBaseURL();
    }

    get<T>(path: string | number): Observable<T> {
        return this._http.get<T>(this.API_URL + this._basePath + path);
    }

    post<T>(body: T, path = ''): Observable<T> {
        return this._http.post<T>(this.API_URL + this._basePath + path, body);
    }

    put<T>(body: T, path = ''): Observable<T> {
        return this._http.put<T>(this.API_URL + this._basePath + path, body);
    }

    delete<T>(path: string | number): Observable<T> {
        return this._http.delete<T>(this.API_URL + this._basePath + path);
    }

    filterPost(path: string, filtro: any, params: HttpParams): Observable<any> {
        return this._http.post<any>(this.API_URL + this._basePath + path, filtro, {params: params});
    }

    filterGet(path: string, params: HttpParams): Observable<any> {
        return this._http.get<any>(this.API_URL + this._basePath + path,{params: params});
    }

    private createBaseURL(): string {
        const server = this.configuration.server;
        return server.protocol + '://' + server.host + ':' + server.port;
    }

    get basePath(): string {
        return this._basePath;
    }

    set basePath(value: string) {
        this._basePath = value;
    }

}
