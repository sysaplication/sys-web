import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Template criado por<b><a href="https://akveo.com" target="_blank">Akveo</a></b> 2019 - Utilizado na prova técnica</span>
  `,
})
export class FooterComponent {
}
