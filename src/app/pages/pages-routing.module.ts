import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'situacao',
      loadChildren: () => import('./situacao/situacao.module')
        .then(m => m.SituacaoModule),
    },
    {
      path: 'uf',
      loadChildren: () => import('./uf/uf.module')
        .then(m => m.UfModule),
    },
    {
      path: 'pessoa',
      loadChildren: () => import('./pessoa/pessoa.module')
        .then(m => m.PessoaModule),
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'pessoa',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
