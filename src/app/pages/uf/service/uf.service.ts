import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {PageModel} from '../../../@core/model/page.model';
import {Observable} from 'rxjs';
import {ApiService} from "../../../@core/services/api.service";

@Injectable({
  providedIn: 'root'
})
export class UfService extends ApiService {

  constructor(public http: HttpClient) {
    super(http, '/api/ufs/');
  }

  recuperarTodos(page: PageModel): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', ''+page.page);
    params = params.append('size', ''+page.size);
    return this.filterGet('',params);
  }

}
