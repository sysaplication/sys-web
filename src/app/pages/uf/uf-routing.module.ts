import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UfComponent } from './uf.component';
import {ConsultaComponent} from "./consulta/consulta.component";

const routes: Routes = [
  {
    path: '',
    component: UfComponent,
    children: [
      {
        path: 'consultar',
        component: ConsultaComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class UfRoutingModule {
}

