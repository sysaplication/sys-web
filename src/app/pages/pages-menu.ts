import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Pessoa',
    icon: 'person-outline',
    link: '/pages/pessoa/consultar',
    home: true,
  },
  {
    title: 'Administrativo',
    group: true,
  },
  {
    title: 'Parâmetros',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'Situação',
        link: '/pages/situacao/consultar',
      },
      {
        title: 'UF',
        link: '/pages/uf/consultar',
      }
    ],
  }
];
