import {NgModule} from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import {ThemeModule} from '../../@theme/theme.module';
import {PessoaRoutingModule} from './pessoa-routing.module';
import {PessoaComponent} from './pessoa.component';
import {FormsModule, FormsModule as ngFormsModule, ReactiveFormsModule} from '@angular/forms';
import {CadastroComponent} from './cadastro/cadastro.component';
import {NgBootstrapFormValidationModule} from 'ng-bootstrap-form-validation';
import {ConsultaComponent} from './consulta/consulta.component';
import {NgxMaskModule} from 'ngx-mask';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports: [
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NgxMaskModule.forRoot(),
    NbRadioModule,
    NbDatepickerModule,
    PessoaRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule,
    NgxPaginationModule,
  ],
  declarations: [
    PessoaComponent,
    CadastroComponent,
    ConsultaComponent,
  ],
})
export class PessoaModule { }
