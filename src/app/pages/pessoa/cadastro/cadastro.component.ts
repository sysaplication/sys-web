import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {NbToastrService} from '@nebular/theme';
import {ToasterService} from '../../../@core/services/toaster.service';
import {PessoaService} from '../service/pessoa.service';
import {PessoaModel} from '../model/pessoa.model';
import {UfService} from '../../uf/service/uf.service';
import {UfModel} from '../../uf/model/uf.model';
import {SituacaoModel} from '../../situacao/model/situacao.model';
import {SituacaoService} from '../../situacao/service/situacao.service';
import {DateValidator} from '../../../@core/validator/date.validator';
import {PageModel} from '../../../@core/model/page.model';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./cadastro.component.scss'],
  templateUrl: './cadastro.component.html',
})
export class CadastroComponent {

  formPessoa: FormGroup;

  ufs: UfModel[];
  situacoes: SituacaoModel[];

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    private  toasterService: ToasterService,
    private toastrService: NbToastrService,
    private ufService: UfService,
    private situacaoService: SituacaoService,
    private pessoaService: PessoaService,
    private formBuilder: FormBuilder
  ) {
    this.construirForm();
    this.init();
    this.recuperarUFs();
    this.recuperarSituacoes();
  }

  init() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['id']) {
        this.pessoaService.recuperarPorId(params['id'])
          .subscribe((retorno) => {
            this.construirForm(retorno);
          }, error => {
            this.toasterService.showToast( 'danger', 'Operação não realizada', '');
          });
      }
    });
  }

  construirForm(pessoaModel?: PessoaModel) {
    this.formPessoa = this.formBuilder.group({
      id: [pessoaModel != null ? pessoaModel.id : null, []],
      nome: [pessoaModel != null ? pessoaModel.nome : null, [Validators.required, Validators.maxLength(70)]],
      dataNascimento: [pessoaModel != null ? pessoaModel.dataNascimento : null, [Validators.required, DateValidator.ptDate]],
      celular: [pessoaModel != null ? pessoaModel.celular : null, [Validators.required]],
      uf: [pessoaModel != null ? pessoaModel.uf : null, []],
      situacao: [pessoaModel != null ? pessoaModel.situacao : null, [Validators.required]],
    });
  }

  cadastro() {
    if (this.formPessoa.value.id) {
      this.pessoaService.put(this.formPessoa.value)
        .subscribe(() => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.irParaTelaDeConsulta();
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
    } else {
      this.pessoaService.post(this.formPessoa.value)
        .subscribe(() => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.irParaTelaDeConsulta();
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
    }
  }

  irParaTelaDeConsulta() {
    this.router.navigate(['/pages/pessoa/consultar']);
  }

  limpar() {
    this.construirForm();
  }

  recuperarUFs(value?: string) {
    this.ufService
      .recuperarTodos(new PageModel())
      .subscribe(
        (retorno) => {
          this.ufs = retorno.content;
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  recuperarSituacoes(value?: string) {
    this.situacaoService
      .recuperarTodos(new PageModel())
      .subscribe(
        (retorno) => {
          this.situacoes = retorno.content;
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  compareFn(c1, c2): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  ativar() {
    this.formPessoa.get('situacao').setValue(new SituacaoModel(1))
  }
}
