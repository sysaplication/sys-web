import {UfModel} from '../../uf/model/uf.model';
import {SituacaoModel} from '../../situacao/model/situacao.model';

export class PessoaModel {

  id: number;
  nome: string;
  celular: string;
  dataNascimento: string;
  uf: UfModel;
  situacao: SituacaoModel;
}
