import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NbToastrService} from '@nebular/theme';
import {ToasterService} from '../../../@core/services/toaster.service';
import {PessoaService} from "../service/pessoa.service";
import {PageModel} from "../../../@core/model/page.model";

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./consulta.component.scss'],
  templateUrl: './consulta.component.html',
})
export class ConsultaComponent {

  retorno :PageModel = new PageModel(5);
  paginaAtual: number = 1;
  termoDeBusca: string = '';

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    private  toasterService: ToasterService,
    private toastrService: NbToastrService,
    private pessoaService: PessoaService,
    private formBuilder: FormBuilder
  ) {
    this.recuperarPessoas(this.retorno);
  }


  recuperarPessoas(page?: PageModel, termo?: string) {
    this.pessoaService
      .recuperarTodos(page, termo)
      .subscribe(
        (retorno) => {
          if (!retorno.content) {
            retorno.content = [];
          }
          this.retorno = retorno;
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  excluir(value?: number) {
    this.pessoaService
      .delete(value)
      .subscribe(
        (retorno) => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.recuperarPessoas(this.retorno, this.termoDeBusca);
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  paginar(event) {
    this.paginaAtual = event;
    this.retorno.page = event - 1;
    this.recuperarPessoas(this.retorno, this.termoDeBusca);
  }

  pesquisar() {
    this.recuperarPessoas(this.retorno, this.termoDeBusca);
  }

  limpar() {
    this.termoDeBusca = '';
    this.recuperarPessoas(this.retorno);
  }
}
