import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PessoaComponent } from './pessoa.component';
import {CadastroComponent} from './cadastro/cadastro.component';
import {ConsultaComponent} from './consulta/consulta.component';

const routes: Routes = [
  {
    path: '',
    component: PessoaComponent,
    children: [
      {
        path: 'cadastrar',
        component: CadastroComponent,
      },
      {
        path: 'editar/:id',
        component: CadastroComponent,
      },
      {
        path: 'consultar',
        component: ConsultaComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class PessoaRoutingModule {
}

