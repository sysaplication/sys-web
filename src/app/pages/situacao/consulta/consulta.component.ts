import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {SituacaoService} from '../service/situacao.service';
import {NbToastrService} from '@nebular/theme';
import {ToasterService} from '../../../@core/services/toaster.service';
import {PageModel} from '../../../@core/model/page.model';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./consulta.component.scss'],
  templateUrl: './consulta.component.html',
})
export class ConsultaComponent {

  retorno: PageModel = new PageModel(5);
  paginaAtual: number = 1;

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    private  toasterService: ToasterService,
    private toastrService: NbToastrService,
    private situacaoService: SituacaoService,
    private formBuilder: FormBuilder
  ) {
    this.recuperarSituacoes(this.retorno);
  }


  recuperarSituacoes(page?: PageModel) {
    this.situacaoService
      .recuperarTodos(this.retorno)
      .subscribe(
        (retorno) => {
          this.retorno = retorno;
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  excluir(value?: number) {
    this.situacaoService
      .delete(value)
      .subscribe(
        (retorno) => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.recuperarSituacoes();
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
  }

  paginar(event) {
    console.info(event)
    this.paginaAtual = event;
    this.retorno.page = event - 1;
    this.recuperarSituacoes(this.retorno);
  }

}
