import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SituacaoModel} from '../model/situacao.model';
import {SituacaoService} from '../service/situacao.service';
import {NbToastrService} from '@nebular/theme';
import {ToasterService} from '../../../@core/services/toaster.service';

@Component({
  selector: 'ngx-form-inputs',
  styleUrls: ['./cadastro.component.scss'],
  templateUrl: './cadastro.component.html',
})
export class CadastroComponent {

  formSituacao: FormGroup;

  constructor(
    protected route: ActivatedRoute,
    private router: Router,
    private  toasterService: ToasterService,
    private toastrService: NbToastrService,
    private situacaoService: SituacaoService,
    private formBuilder: FormBuilder
  ) {
    this.construirForm();
    this.init();
  }

  init() {
    this.route.queryParams.subscribe((params: Params) => {
      if (params['id']) {
        this.situacaoService.recuperarPorId(params['id'])
          .subscribe((retorno) => {
            this.construirForm(retorno);
          }, error => {
            this.toasterService.showToast( 'danger', 'Operação não realizada', '');
          });
      }
    });
  }

  construirForm(situacaoModel?: SituacaoModel) {
    this.formSituacao = this.formBuilder.group({
      id: [situacaoModel != null ? situacaoModel.id : null, []],
      descricao: [situacaoModel != null ? situacaoModel.descricao : null, [Validators.required, Validators.maxLength(25)]],
    });
  }

  cadastro() {
    if (this.formSituacao.value.id) {
      this.situacaoService.put(this.formSituacao.value)
        .subscribe(() => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.irParaTelaDeConsulta();
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
    } else {
      this.situacaoService.post(this.formSituacao.value)
        .subscribe(() => {
          this.toasterService.showToast( 'success', 'Operação realizada com sucesso', '');
          this.irParaTelaDeConsulta();
        }, error => {
          this.toasterService.showToast( 'danger', 'Operação não realizada', '');
        });
    }
  }

  irParaTelaDeConsulta() {
    this.router.navigate(['/pages/situacao/consultar']);
  }

  limpar() {
    this.construirForm();
  }

}
