import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {PageModel} from '../../../@core/model/page.model';
import {Observable} from 'rxjs';
import {ApiService} from "../../../@core/services/api.service";

@Injectable({
  providedIn: 'root'
})
export class SituacaoService extends ApiService {

  constructor(public http: HttpClient) {
    super(http, '/api/situacoes/');
  }

  recuperarTodos(page: PageModel): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', ''+page.page);
    params = params.append('size', ''+page.size);
    return this.filterGet('',params);
  }

}
