import {NgModule} from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';

import {ThemeModule} from '../../@theme/theme.module';
import {SituacaoRoutingModule} from './situacao-routing.module';
import {SituacaoComponent} from './situacao.component';
import {FormsModule, FormsModule as ngFormsModule, ReactiveFormsModule} from '@angular/forms';
import {CadastroComponent} from './cadastro/cadastro.component';
import {NgBootstrapFormValidationModule} from 'ng-bootstrap-form-validation';
import {ConsultaComponent} from './consulta/consulta.component';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports: [
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    SituacaoRoutingModule,
    NbSelectModule,
    NbIconModule,
    ngFormsModule,
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule,
    NgxPaginationModule
  ],
  declarations: [
    SituacaoComponent,
    CadastroComponent,
    ConsultaComponent
  ],
})
export class SituacaoModule {
}
